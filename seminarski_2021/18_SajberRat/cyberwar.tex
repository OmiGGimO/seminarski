% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[utf8]{inputenc} % make weird characters work
\usepackage{graphicx}

\usepackage[english,serbian]{babel}
%\usepackage[english,serbianc]{babel} %ukljuciti babel sa ovim opcijama, umesto gornjim, ukoliko se koristi cirilica

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

%\newtheorem{primer}{Пример}[section] %ćirilični primer
\newtheorem{primer}{Primer}[section]

\begin{document}

\title{Sajber rat\\ \small{Seminarski rad u okviru kursa\\Računarstvo i društvo\\ Matematički fakultet}}

\author{Katarina Simić\\ simickata@gmail.com}
\date{17.~april 2021.}
\maketitle

\abstract{
U ovom tekstu je obrađen članak \cite{Cyberwar}. Dati su odgovori na pitanja kako definisati sajber rat, 
kada, kako i zašto se on javlja. Pokazani su neki (navodni) primeri sajber ratovanja.
Predstavljene su neke metode koje se koriste za napade u sajber prostoru, kao i uzorak heurističkog dijagrama za klasifikaciju tipova sajber napada.
Prikazane su najpopularnije vrste napada, kao i lista nivoa na kojima se sajber napadi pokreću.
\newline
Sajber rat može ukjlučivati i kinetičke i ne--kinetičke aktivnosti, razmotreno je koja je mera da se
nekinetičke aktivnosti smatraju sajber ratovanjem kada nisu povezane sa stvarnom fizičkom bitkom.
\newline
Čini se da se ne--kinetički sajber napadi povećavaju i učestalošću i težinom u pogledu potencijalne štete koju prouzrokuju.
Nacionalne države su optuživane za korišćenje sajber napada, kako tokom, tako i u odsustvu kinetičke bitke.
Trenutno je najveća poteškoća identifikovanje počinilaca sa sigurnošću.
}

\tableofcontents

\newpage

\section{Uvod}
\label{sec:uvod}
Sajber rat je potajan i nevidljiv za većinu. Odvija se u sajber--prostoru, mestu koje se ne može videti, dodirnuti niti osetiti. 
Sajber--prostor se definiše kao peto ratno područje, posle kopna, mora, vazduha i svemira \cite{DCAF Horizons}.
Njega čine sve računarske mreže na svetu.
Možemo videti sve fizičke instrumente rata, međutim, ovi instrumenti komuniciraju u virtuelnom i nevidljivom svetu.
\newline
Šta je sajber rat? Da li ćemo ga prepoznati kad ga vidimo? Ako prepoznamo, šta treba učiniti?
\newline
Ova pitanja mogu postati vrlo bitna za život u 21. veku, a manjak preciznosti u terminologiji pomaže u njihovom zamagljivanju.
Termin sajber rat je korišćen u mnogim različitim kontekstima.
Sa obzirom na to da se sam rat smatra vojnim poduhvatom, sajber rat se često povezuje sa konceptualnim okvirom tradicionalnih pojmova ratovanja.
Ovi pojmovi generalno uključju silu, nasilje i fizičke povrede.
U ovom radu ispituju se izazovi koje ova definicija predstavlja u 21. veku sajber povezanog i sajber zavisnog sveta, 
i predstavljen je prošireni konceptualni okvir za sajber rat.
\newline
Nivo aktivnosti ili ponašanje uključeno u sajber rat, količina i tip sajber napada, postaju bitni faktori pri definisanju toga da li to jeste sajber rat.
U prepoznavanju uloge koju ce sajber napadi imati u budućim vojnim sukobima,
identifikovana su dva pitanja kada nacionalne države procenjuju posledice i svoj potencijalni odgovor na sajber rat.
Prvo, koji je prag da se sajber događaj smatra ratnim činom ili da može da se poredi
sa upotrebom sile?
Drugo, koji je prag između taktičke i strateške primene sajber napada? \cite{Cyberwar thresholds and effects}
\newline
Sajber rat može ukjlučivati i kinetičke i nekinetičke aktivnosti. Kinetičke aktivnosti se povezuju sa kretanjem.
U vojnom području, ovo tipično uključuje oružane napade, bacanje bombi, itd. 
Nekinetičke sajber ratne aktivnosti su tipično usmerene ka bilo kakvom aspektu suparničkih
sajber sistema, kao što su komunikacija, logistika ili obaveštajni podaci.
\newline
Kada se kombinuju zajedno sa kinetičkom borbom, nekinetičke sajber aktivnosti mogu uključiti poremećaj protivničkog
logističkog lanca snabdevanja ili preusmeravanje osnovnih vojnih zaliha.
Neki drugi tipovi nekinetičke sajber aktivnosti su: destablilizacija upravnog finansijskog sistema, 
upad u upravne računarske sisteme, ili infiltriranje računarskih sistema sa ciljem špijunaze.
U daljem tekstu razmatraće se koja je mera da se ove nekinetičke aktivnosti smatraju sajber ratovanjem kada nisu
povezane sa stvarnom fizičkom bitkom.


\newpage


\section{Kako definisati sajber rat}
\label{sec:kako definisati sajber rat}
Poteškoca u definisanju sajber rata je u tome što nacionalne države i nedržavni akteri ne poštuju uvek zakon kada je u pitanju rat.
Štavise, zbog porasta asimetričnog ratovanja\footnote{Ratovanje koje ukljucuje iznenadne napade malih, jednostavno naoružanih grupa na državu naoružanu modernim visokotehnološkim oružjem.},
i eksponencijalnog razvoja interneta sajber napadi imaju tendenciju da budu rasprostranjeniji. 
U ovakvom okruženju, uticaj zakona o sajber ratovanju, kao regulatornom mehanizmu, može stoga biti ograničen.
Talinski priručnik\footnote{Talinski priručnik o međunarodnom pravu koji se primenjuje u sajber ratovanju, napisan na zahtev NATO i 
Centra izvrsnosti za sajber odbranu (CCD--COE).}
definiše sajber rat kao sajber napad, u odbranbenoj ili napadnoj sajber operaciji,
koji rezultuje u nasilju, smrti i/ili destrukciji \cite{Tallinn Manual}.
Ova definicja ima nedostatke, na primer, isključuje sajber operacije dizajnirane da destabilizuju finansijski sistem nacionalne države, 
s obzirom na to da napad nije direktno uzrokovao smrt niti fizičku desturkciju.
Tradicionalno, nasilje se smatra neophodnim delom sajber napada, stavljajući sajber rat u kontekst oružanog napada.
Fokus u ovoj definiciji bio je jednakost efekta sajber napada sa efektom oružanog napada korišćenjem fizičkih sredstava \cite{Cyberwar thresholds and effects}.
\newline
Ovaj pristup sajber ratu su prilagodili oni koji sajber napade u vojnim kampanjama smatraju motivom za ciljanje protivničkih komunikacija,
obaveštajnih podataka, kao i drugih logističkih operacija zasnovanih na Internetu ili mreži \cite{Declarations of cyberwar}.
\newline
Povezanost sajber rata sa upotrebom sile i oružanim sukobom može biti trenutna prevladavajuća pozicija u nekim međunarodnim sektorima.
Međutim, ne uzima se u obzir obim nefizičke štete koja može biti naneta kroz sajber prostor u svetu koje je
sve vise umrežen, koji može dostići i nuklearna postrojenja.
\newline
Ženevski centar za demokratsku kontrolu oružanih snaga (DCAF) usvojio je sveobuhvatniju definiciju sajber napada u svom radnom dokumentu DCAF Horizons 2015.
Ova definicija pravi razliku između sajber napada koje sponzoriše država i nedržavnih sajber napada,
a takođe uključuje i sajber vandalizam, sajber kriminal i sajber špijunažu u okviru svoje definicije sajber napada \cite{DCAF Horizons}.
DCAF definiše sajber rat kao ratno ponašanje koje se sprovodi u virtuelnom svetu koristeći informacije,
komunikacionu tehnologiju i mreže, sa namerom da poremeti ili uništi neprijateljske informacione i komunikacione sisteme.
Cilj je uticaj na sposobnost odlučivanja protivničkog političkog rukovodstva i oružanih snaga \cite{DCAF Horizons}.
Stoga se razlikuje u dve ključne oblasti.
Prvo, prepoznaje da postoji nefizički uticaj na sajber rat,
i drugo, prepoznaje značaj političkih lidera u donošenju ove odluke.
Čista vojno--ciljna definicija sajber rata više nije realna u kontekstu savremenih geo--političkih nestabilnosti 
i globalnog okruženja asimetričnog ratovanja. Kada je manja sila u sukobu sa većim entitetom, 
oružani sukob najverovatnije neće biti uspešan za manju silu.
Pored toga, stvarnost sukoba dokazuje da su odluke o tome kada će nacionalna država objaviti rat, 
kao i prethodna interpretacija događaja koji su doveli do tog opredeljenja, 
odluke koje donosi njeno politicko rukovodstvo.
Kao rezultat toga, pojmovi sajber napad i sajber rat moraju se razdvojiti tako da sajber napadi ne budu definisani isključivo u
smislu upotrebe ili dejstva fizičke sile koja uzrokuje smrt, štetu ili uništenje. Ili, ako će pojmovi sajber napad i sajber rat 
i dalje biti sinonimi, onda je važno priznati da sajber napadi, a samim tim i sajber rat, 
mogu uključivati isključivo ne--kinetičku sajber aktivnost bez kinetičke vojne akcije.


\newpage

\section{Kada se javlja sajber rat}
\label{sec:kada se javlja sajber rat}
 
Praktično je nemoguće identifikovati svaki sajber napad koji se dogodi. 
Neki mogu funkcionisati neotkriveni godinama. Drugi su kratki, ali ne ostavljaju tragove pomoću kojih se mogu identifikovati.
Ovaj odeljak opisuje evropski napor usmeren ka merenju učestalosti i izvora pokušaja infiltracije tokom jednog meseca. 
Takođe opisuje nekoliko odabranih globalnih primera sajber napada.
Rastuća zabrinutost zbog sigurnosti Sistema Nadzorne Kontrole i Prikupljanja Podataka (SCADA) razmatrana je kasnije u ovom članku.


\section{Učestalost sajber napada}
\label{sec:učestalost sajber napada}

Deutsche Telekom AG (DTAG), nemačka kompanija za telekomunikacije, uspostavila je mrežu od 97 senzora koji
služe kao sistem ranog upozorenja koji će u realnom vremenu pružiti sliku o tekućim sajber napadima. 
Iako je većina senzora smeštena u Nemačkoj, DTAG takođe locira honeypots\footnote{Honeypot je računarski sigurnosni mehanizam postavljen za otkrivanje, uklanjanje ili, na neki način, suzbijanje pokušaja neovlašćene upotrebe informacionih sistema.
} 
i senzore u drugim neevropskim zemljama.
\newline
Top petnaest zemalja koje su DTAG senzori zabeležili kao izvor sajber napada istaknuti su na slici 1. 
Otprilike 20\% navedenih sajber napada bilo je poreklom iz Ruske Federacije. 
Prve četiri navedene države, uključujući SAD, Nemačku i Tajvan, činile su 62\% zastupljenih sajber napada.
Ovi slučajevi pružaju sliku napada usmerenih na određeno geografsko područje, u ovom slučaju Evropu.

\begin{verbatim}
\end{verbatim}

\begin{figure}[h!]
  \centering
  \begin{center}
  \includegraphics[width=55mm]{slika1.jpg}
  \end{center}
  \caption{Top 15 zemalja izvora za sajber napade u maju 2013. \cite{Overview of current cyber attacks}}
  \label{fig:vr1}
\end{figure}

\newpage

Na širem međunarodnom i istorijskom nivou, Radni dokument DCAF Horizons 2015 opisuje istorijske primere onoga
što oni identifikuju kao sajber sukob i koje bi očigledno trebalo smatrati sajber napadima.
\newline
Napadi su sažeti u slici 2. Treba napomenuti da je za mnoge od opisanih sajber napada počinitelj naveden kao „navodni“.
Ovo odražava poteškoće u utvrđivanju odgovornosti.


\begin{figure}[h!]
  \centering
  \begin{center}
  \includegraphics[width=100mm]{slika2_1.jpg}
  \end{center}
  \caption{Istorija sajber napada prema izveštaju Centra za demokratsku kontrolu oružanih snaga (DCAF) \cite{DCAF Horizons}}
  \label{fig:vr2}
\end{figure}

  
\begin{figure}[h!]
  \centering
  \begin{center}
  \includegraphics[width=100mm]{slika2_2.jpg}
  \end{center}
  \caption{Istorija sajber napada prema izveštaju Centra za demokratsku kontrolu oružanih snaga (DCAF)--nastavak \cite{DCAF Horizons}}
  \label{fig:vr3}
\end{figure}

Od četrnaest sajber napada prikazanih na slici 2, pet se dogodilo u kontekstu stvarnog kinetičkog ili „vrućeg“ rata, 
jedan se dogodio u kontekstu „hladnog“ rata, a ostatak se dogodio u kontekstu tekućih tenzija između nacionalnih država, 
ili između nacionalne države i nedržavnih aktera koje druga nacionalna država može ili ne mora podržati.
\newline
Privremeni trend u ovim identifikovanim sukobima je korišćenje sajber napada u odsustvu kinetičke bitke. 
Kada se posmatraju sa naknadnim pojavljivanjima sajber napada opisanim u slici 3, trend je ka napadima na kritičnu infrastrukturu nacionalne države \cite{Principles of Cybercrime}.


\begin{figure}[h!]
  \centering
  \begin{center}
  \includegraphics[width=100mm]{slika3.jpg}
  \end{center}
  \caption{Nedavni sajber napadi na kritičnu infrastrukturu \cite{The Flame Virus, Middle East cyber attacks on US banks, In cyberattack on Saudi firm, Cyberattacks against U.S. corporations}}
  \label{fig:vr4}
\end{figure}

\newpage

\section{Zašto se javlja sajber rat}
\label{sec:zašto se javlja sajber rat}

Za manje države ili terorističke organizacije upotreba DDoS napada\footnote{DoS (Denial of Service) je napad na neki kompjuterski servis s ciljem da se
korisnicima onemogući njegovo korišćenje, ovaj pojam najčešće čujemo u kontekstu napada na neku veb stranicu. Ukoliko se takav napad izvede sa većeg broja
računara, onda se on zove DDoS (Distributed Denial of Service) i mnogo je efikasniji. DDoS napadi se danas najčešće izvode putem tzv. botneta.} 
mnogo je jeftinija za pokretanje od konvencionalnih ratnih
alata protiv neprijatelja koji poseduju veće resurse u pogledu oružja, novca i trupa. 
Zamislite bespilotnu letelicu (dron), ne samo konfiskovanu, već i preusmerenu nazad prema svom pokretaču.
Potrebno je manje resursa, ali, s druge strane, potrebna je povećana specijalizovana obuka. 
\newline
Sajber napadač za iznajmljivanje je profitabilan posao za one koji su ranije bili samo sajber kriminalci.
Kao što su primetili mnogi, sajber kriminalci mogu postati sajber ratnici za iznajmljivanje \cite{The Next Threat to National Security}. 
Ovaj lagani prelazak sa sajber kriminala na najam sajber ratnika sugeriše to oslanjanje na strogo razgraničavanje između dve aktivnosti. 
Sajber kriminal i sajber napadi mogu dugoročno dovesti do povećanih sajber napada.
Sajber napadi imaju sposobnost da poremete način na koji obični pojedinci žive svoj život 
(npr. haos koji bi nastao da nijedan bankomat u nekoj zemlji ne radi).
Međusobna povezanost globalnih finansijskih institucija, omogućena modernom komunikacionom tehnologijom, povećava ovaj rizik \cite{Technology as Risk}.
\newline 
Pre nekoliko godina bili smo svedoci uticaja zamračenja (blackout--a) u SAD--u 2003. godine koje je uticalo na elektronske mreže od Ohaja do Njujorka, pa čak i do kanadske provincije Ontario.
Iako je trajalo samo nekoliko dana, važno je pitati se kakav bi bio uticaj nečeg sličnog, ili u većem obimu, 
namerno izazvanog tokom dužeg vremenskog perioda. 
\newline
Kada je Estonija bila izložena nizu sajber napada, bila je primorana da prekine spoljne Internet veze kako bi ljudi u zemlji mogli da nastave da koriste svoje svakodnevne usluge\cite{Estonia under cyberassault}. Sa onemogućenim pristupom spoljnim Internet uslugama, estonski putnik u drugoj zemlji nije mogao da preuzme novac sa bankomata ili da koristi kreditnu karticu koju je izdala banka. Uprkos tome što je 30 000 računara bilo isključeno sa mreže, virus Sharmoon na kraju nije bio uspešan time što nije značajno poremetio proizvodnju nafte ni u Saudijskoj Arabiji ni u Kataru.
Ali, šta da jeste? Kakvi bi bili uticaji?
\newline
Sjedinjene Američke Države su identifikovale sajber napade na svoju kritičnu infrastrukturu kao pitanje nacionalne bezbednosti
i proglasile sajber prostor domenom rata \cite{The Threat in Cyberspace}. Kritične infrastrukture su fizički ili virtuelni sistemi i sredstva 
koja su toliko bitna za naciju da bi svaka šteta koja im se nanese imala drastičan efekat na sigurnost, nacionalnu ekonomsku 
sigurnost, nacionalno javno zdravlje ili bezbednost \cite{Improving critical infrastructure cybersecurity}.
Konkretno, ovi napadi se odnose na poljoprivredu, hranu, vodu, javno zdravstvo, hitne službe, vladu, odbranu, 
industrijsku bazu, informacije i telekomunikacije, energiju, transport, bankarstvo i finansije, hemijsku industriju i poštanske i brodske sisteme \cite{Critical infrastructures}.
Potencijalni destabilizirajući efekti prekida na ovoj infrastrukturi tiču se donosilaca političkih odluka koji 
takve prekide na kraju označavaju kao sajber napad ili sajber rat.
Zbog toga se sajber napadi, ili sajber rat, često šire i izvan fizičkog bojnog polja. 
Iako gore pomenuti poremećaji ne uzrokuju fizičku povredu ili štetu, ipak se mogu smatrati ratnim dejstvom.



\section{Kako se javlja sajber rat}
\label{sec:kako se javlja sajber rat}

Sajber napadi koriste razne vektore, kako tehnološke tako i organizacione. 
Oni traže ranjivosti u bilo kom od entiteta koji čine sajber prostor. 
Otkriveno da je veća verovatnoća da će određene vrste napada poteći iz određenih nacija ili regiona.
Na primer, 75\% dobavljača Internet usluga (ISP) koji sadrže najviše phishing\footnote{Lažna praksa slanja elektronskih poruka sa navodnim uvaženim kompanijama kako bi se naveli pojedinci da otkriju lične podatke, kao što su lozinke i brojevi kreditnih kartica.} 
prevara nalaze se u Sjedinjenim Državama. 
Obična neželjena pošta (spam) prvenstveno potiče iz Indije i Vijetnama, dok je najveća koncentracija neželjene pošte 
po Internet adresi u Nigeriji \cite{Internet Bad Neighborhoods}. Tvrdi se da bi analiza mesta koncentracije zlonamernih domaćina mogla poboljšati predviđanje budućih napada.
\newline
Za pokretanje napada u sajber prostoru koristi se nekoliko tehnoloških metoda. U ovom odeljku ukratko su predstavljene neke metode
koje se koriste za napade u sajber prostoru, 
takođe, i uzorak heurističkog dijagrama za klasifikaciju tipova sajber napada.


\section{Metodološki pristupi}
\label{subsec:metodološki pristupi}
DTAG honeypot sistem takođe je identifikovao pet najpopularnijih vrsta napada otkrivenih u maju 2013.
Oni su imali tendenciju da budu usmereni ka sajber ili internet tehnologijama. Kao što je prikazano na slici 4,78\% 
ovih napada bilo je na SMB protokolima (Server Message Block).
SCADA sistemi su posebno osetljivi na napade, a samim tim i privlacni za potencijalne sajber napadače.
Poznati kao „radni konji informatičkog doba“(workhorses of the information age), računarski sistemi upravljanja takođe 
su slaba karika u sistemima kritične infrastrukture \cite{The Threat in Cyberspace, Principles of Cybercrime}. Ovi sistemi regulišu rad infrastrukture.
Na primer, oni upravljaju protokom prirodnog gasa kroz cevovod, ili upravljaju proizvodnjom hemikalija, itd.
SCADA sistemi su sve više povezani sa drugim mrežama, uključujući Internet, što ih čini ranjivim na spoljne sajber napade.
S obzirom na obim štete, poput ozbiljnih povreda, smrtnih slučajeva, nedostupnosti presudnih svakodnevnih usluga 
koje mogu nastati ako se poremeti rad SCADA sistema, nije iznenađujuće što se ovi napadi smatraju sajber terorizomom ili sajber ratom. SCADA sistemi upravljaju električnim mrežama, otvaraju i zatvaraju brane, regulišu mnoštvo drugih vitalnih, kritičnih infrastrukturnih operacija.
Upravo je SCADA sistem u iranskom postrojenju za nuklearnu centrifugu uspešno bio na meti Stuksneta (virus).
\newline
Opasnost za visoko industrijske zemlje je što ih njihova kompjuterizovana kritična infrastruktura čini ranjivim na slične napade. 
Sjedinjene Američke Države imaju jedan od najrazvijenijih, kompjuterizovanih sistema kritične infrastrukture na svetu, sto ih čini izuzetno ranjivim\cite{The Next Threat to National
Security}. 


\begin{figure}[h!]
  \centering
  \begin{center}
  \includegraphics[width=100mm]{slika4.jpg}
  \end{center}
  \caption{Top 5 tipova napada maja 2013. godine \cite{Overview of current cyber attacks}}
  \label{fig:vr5}
  \end{figure}

  \newpage

Ponavljajuće pitanje je da li postoje slučajevi kada bi se DDoS napadi mogli smatrati ratnim dejstvom. 
Sajber rat može razumeti u kontekstu pomorskog trgovinskog rata \cite{Cyberwarfare seen through a mariner’s spyglass}.
Plovni putevi su najefikasniji mehanizam za prevoz opipljive robe. Internet je najefikasniji mehanizam za prevoz nematerijalne robe.
Stoga, kao što su se u oba svetska rata pomorske blokade i napadi na brodske trake smatrali ratnim dejstvima, 
jer su sprečavali prevoz opipljive robe, tako i DDoS napadi blokiraju transport nematerijalne robe \cite{Cyberwarfare seen through a mariner’s spyglass}. 
Ova definicija, iako je još uvek analogna tradicionalnom ratovanju, pruža definiciju sajber rata, 
koji zaobilazi neophodne uslove kinetičke bitke.
\newline
Manipulacija sve automatizovanijim informacionim sistemima, nesigurnost lanca snabdevanja i zlonamerni softver na više platformi
su među novonastalim sajber pretnjama \cite{­Emerging Cyber Threats Report}. 
Sposobnost manipulacije automatizovanim informacionim sistemima 
direktna je pretnja sigurnosti lanca snabdevanja bilo koje države. Malo ljudi je svesno u kojoj meri je agrobiznis, 
proces kojim se hrana bere, transportuje i prodaje u prodavnicama, automatizovan.
U kombinaciji sa sistemom dizajniranim za isporuku proizvoda i zaliha u prodavnice, 
manipulacija informacionim sistemom distributera može dovesti do toga da isporuke ne budu izvršene. 
To potencijalno može dovesti do nestašice hrane u nekim krajevima. 
\newline
Zlonamerni softver na više platformi takođe je postao češći. To se delimično može pripisati brzom povećanju broja pametnih telefona
i drugih ručnih mobilnih uređaja, zajedno sa pojavom aplikacija. Mnoge aplikacije su dizajnirane da zaraze uređaj i
prenose zlonamerni softver bežično na druge uređaje. Nepovezane sa sajber napadima, prisutne su
i unutrašnje ranjivosti krajnjih korisnika koje se suočavaju sa kritičnom infrastrukturom.

\section{Heuristička klasifikacija sajber napada}
\label{sec:heuristička klasifikacija sajber napada}

Sajber napadi se pokreću na više nivoa. Ova lista nije hijerarhijska ni sveobuhvatna. Među nivoima na kojima može doći do sajber napada su:
\begin{itemize}
\item{Vlada naspram vlade (u kontekstu kinetičke bitke);}\
\item{Asimetrično ratovanje: nedržavni akter\footnote{Npr. terorističke grupe, kompanije, političke ili ideološke ekstremističke grupe, teroristi haktivisti i transnacionalne kriminalne organizacije.}
 protiv sopstvenih agencija ili dobavljača, ili druge vlade;}\
\item{Vlada protiv kritične infrastrukture druge vlade (nekinetička bitka);}\
\item{Krivično nadahnuti hakeri naspram pojedinačnih korisnika.}\
\end{itemize}
Kao što je prethodno rečeno, sajber napadi između nacionalnih država mogu se dogoditi u kontekstu kinetičkih i nekinetičkih bitaka.
Ovo preklapanje je prikazano na slici 6.


\begin{figure}[h!]
  \centering
  \begin{center}
  \includegraphics[width=100mm]{cbw.jpg}
  \end{center}
  \caption{Sajber napadi i organizaciona tipologija}
  \label{fig:vr6}
  \end{figure}

\newpage

\section{Zaključak: Povećanje nekinetičkih sajber napada}
\label{sec:zaključak:povećanje nekinetičkih sajber napada}
Čini se da se ne--kinetički sajber napadi povećavaju i učestalošću i težinom u pogledu potencijalne štete koju prouzrokuju. 
Naročito postoji strah da će teroristi koji vode asimetrični rat protiv većeg, moćnijeg protivnika koristiti ovaj mehanizam napada. 
Mogu se dogoditi a i ne moraju u kontekstu tradicionalnog kinetičkog rata. 
\newline
Nacionalne države takođe su optuživane za korišćenje sajber napada, kako tokom, tako i u odsustvu kinetičke bitke. 
Dolazi do važne distinkcije da politicke vođe i vojne vođe ne koriste nužno iste definicije. Politički lideri skloniji su
da smatraju ne--kinetičke sajber operacije, ciljanje na vladinu, finansijsku ili drugu kritičnu nacionalnu infrastrukturu kao sajber napade,
a time i sajber rat, čak i u odsustvu upotrebe sile, povrede, smrti ili fizičke štete.
\newline
Trenutno su najveće poteškoće sa kojima se suočavaju nacionalne države i organizacije identifikovanje počinilaca sa sigurnošću.
U odsustvu priznanja, sve što je u početku dostupno su spekulacije. Do danas, uprkos čestim optužbama nacionalnih država o tome ko je šta uradio,
ili retorici političkih lidera tih nacionalnih država, nijedna nacionalna država još uvek nije odgovorila na kinetički
sajber napad kinetičkom operacijom.   
Iako će budućnost sajber ratovanja u 21. veku verovatno pokazivati sajber napade koji se dešavaju, 
ne u vezi sa tradicionalnim oružanim sukobom, već samostalno u ne--kinetičkoj borbi, 
podjednako je verovatno da će ove povećane nekinetičke bitke imati kinetičke posledice.




\newpage

\addcontentsline{toc}{section}{Literatura}
\appendix

\iffalse
\bibliography{seminarski} 
\bibliographystyle{plain}
\fi

\begin{thebibliography}{21}

\bibitem{Cyberwar} Angelyn Flowers, Sherali Zeadally, Cyberwar: The What, When, Why, and How
  Article in IEEE Technology and Society Magazine · September 2014,
  https://technologyandsociety.org/cyberwar-the-what-when-why-and-how/

\bibitem{DCAF Horizons} F. Schreier, On Cyberwarfare: DCAF Horizons 2015 Working
Paper. Geneva: Defense Center for Armed Forces, 2013.

\bibitem{Cyberwar thresholds and effects} J. Lewis, “Cyberwar thresholds and effects,” IEEE Security and
Privacy, pp. 23–29, Sept./Oct. 2011.

\bibitem{Tallinn Manual}M. Schmitt, Ed., Tallinn Manual on The International Law Applicable
to Cyberwarfare, Cambridge, U.K.: Cambridge Univ. Press, 2013.

\bibitem{Declarations of cyberwar} W. Jones, “Declarations of cyberwar: What the revelations about
the U.S.-Israeli origin of Stuxnet mean for warfare,” IEEE Spectrum,
pp. 18, Aug. 2012

\bibitem{Overview of current cyber attacks}Deutsche Telekom AG, “Overview of current cyber attacks;” http://
www.sicherheitstacho.eu/, accessed June 6, 2013.

\bibitem{The Next Threat to National Security}R. Clarke and R. Knake, Cyberwar: The Next Threat to National
Security and What to Do AboutIt. New York, NY: Harper Collins, 2010.

\bibitem{Estonia under cyberassault} M. Lesk, “The new front line: Estonia under cyberassault,” IEEE
Security and Privacy, vol. 5, no. 4, pp. 76–79, July-Aug. 2007.

\bibitem{The Threat in Cyberspace} R. O’Harrow, Jr., Zero Day: The Threat in Cyberspace. New York,
NY: Diversion Books, Washington Post E-Book, 2013.

\bibitem{Improving critical infrastructure cybersecurity}B. Obama, “Executive order 13636: Improving critical infrastruc-
ture cybersecurity,” Federal Register, vol. 78, no. 33, part III, Feb.19,
2013.

\bibitem{Critical infrastructures} D. Warfield, “Critical infrastructures: IT security and threats from
private sector ownership,” Information Security J.: A Global Perspec-
tive, vol. 21, no. 3, pp. 127–136, 2012.

\bibitem{Internet Bad Neighborhoods} G. Moreira Moura, Internet Bad Neighborhoods. The Nether-
lands, University of Twente, dissertation, 2013.

\bibitem{Cyberwarfare seen through a mariner’s spyglass} J. Laprise, “Cyberwarfare seen through a mariner’s spyglass,”
IEEE Technology and Society Mag., vol. 25, no. 3, pp. 26–33, 2006.

\bibitem{­Emerging Cyber Threats Report} Georgia Tech Information Security Center and the Georgia Tech
Research Institute, ­Emerging Cyber Threats Report 2013, presented at the Georgia Tech Cyber Security Summit, 2012.

\bibitem{The Flame Virus} J. Newman, “The Flame Virus: Your FAQS answered,” PC World, May
30, 2012

\bibitem{Middle East cyber attacks on US banks} J. Menn, “Middle East cyber attacks on US banks were highly sophis-
ticated,” Huffington Post, Oct. 2, 2012

\bibitem{In cyberattack on Saudi firm} N. Perlroth, “In cyberattack on Saudi firm, U.S. sees Iran striking back,” NY Times, Oct. 23, 2012;
 http://www.nytimes.com/2012/10/24/business/global/cyberattack-on-saudi-oil-firm-disquiets-us.html?pagewanted=all, accessed June 8, 2013.

\bibitem{Cyberattacks against U.S. corporations} D. Sanger, and N. Perloth, “Cyberattacks against U.S. corporations are
on the rise,” NY Times, May 12, 2013


\bibitem{Technology as Risk} L. Orman, “Technology as Risk,” IEEE Technology and Society
Magazine, pp. 23–31, Summer 2013.

\bibitem{Principles of Cybercrime} J. Clough, Principles of Cybercrime. New York, NY: Cambridge
Univ. Press, 2012.

\end{thebibliography}

\end{document}